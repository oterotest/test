package com.oterotest.prueba.control.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.oterotest.prueba.control.servicio.ICamionService;
import com.oterotest.prueba.control.servicio.ICargueService;
import com.oterotest.prueba.modelo.Camion;
import com.oterotest.prueba.modelo.Cargue;

@CrossOrigin(origins = "http://127.0.0.1:5500")
@RestController
@RequestMapping("/kometsales")
public class RestCargue {

	@Autowired
	private ICargueService operacionCargue;

	@Autowired
	private ICamionService operacionCamion;

	@Autowired
	private JavaMailSender javaMailSender;

	@Async
	@PostMapping("/uploadfile")
	@ResponseBody
	public String cargarArchivo(@RequestParam("file") MultipartFile archivo, String mail) throws IOException , InterruptedException{
		Thread.sleep(60000);
		Cargue informacionCarga = operacionCargue.guardar(archivo, mail);
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(archivo.getInputStream(), StandardCharsets.UTF_8))) {
			HeaderColumnNameMappingStrategy<Camion> strategy = new HeaderColumnNameMappingStrategy<>();
			strategy.setType(Camion.class);
			CsvToBean<Camion> csvToBean = new CsvToBeanBuilder<Camion>(br).withType(Camion.class)
					.withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).build();
			consumirServicioCamion(informacionCarga, csvToBean.parse());
			operacionCargue.modificar(informacionCarga, informacionCarga.getId());
			sendEmail(informacionCarga);
		} catch (Exception e) {
			e.printStackTrace();
			return HttpStatus.PRECONDITION_FAILED + e.getMessage();

		}
		return HttpStatus.OK.toString();

	}

	public void consumirServicioCamion(Cargue infBasicCargue, List<Camion> camiones) {
		int numeroErrores = 0;
		int numeroRegistrosCorrectos = 0;
		for (Camion x : camiones) {
			try {
				operacionCamion.registrar(x, infBasicCargue.getId());
				numeroRegistrosCorrectos++;
			} catch (Exception e) {
				numeroErrores++;
			}
		}
		infBasicCargue.setNumero_error(numeroErrores);
		infBasicCargue.setNumero_exitos(numeroRegistrosCorrectos);
	}


	// @Async("threadPoolTaskExecutor")
	public void sendEmail(Cargue informe) throws InterruptedException, IOException, MessagingException {
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper msg_contenido = new MimeMessageHelper(message, true);
		msg_contenido.setTo(informe.getMail_responsable());
		msg_contenido.setSubject("Reporte de Informe de cargue de Camiones (Lote " + informe.getId() + ")");
		msg_contenido.setText("Cordial saludo \n\nEl presente  correo es para informar"
				+ " que se efectuo un cargue de informacion de camiones con fecha " + informe.getFecha_cargue()
				+ "\n\n\nEl numero de Camiones guardados correctamente fueron : " + informe.getNumero_exitos()
				+ " \nEl numero de Filas erradas fueron :" + informe.getNumero_error() + "\n\n"
				+ "Recuerde que puede modificar la informacion y cargala nuevamente\n\n"
				+ "Mas Detalles  ir a http://localhost:8080/kometsales/camiones/all?lote=" + informe.getId() + "\n\n"
				+ "NOTA: Si al verificar la estructura del archivo este no tiene error de formato, "
				+ "recuerde que el sistema no permite registrar camiones con la misma placa!");
		File temp = new File("file.csv");
		if (!temp.exists())
			temp.createNewFile();
		try (FileOutputStream fileOuputStream = new FileOutputStream(temp)) {
			fileOuputStream.write(informe.getDatos());
		}
		FileSystemResource file = new FileSystemResource(temp);
		msg_contenido.addAttachment("Cargue.csv", file);
		javaMailSender.send(message);

	}

}
