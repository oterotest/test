package com.oterotest.prueba.control.servicio;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oterotest.prueba.control.dao.CamionDAO;
import com.oterotest.prueba.modelo.Camion;

@Service
public class CamionImplService implements ICamionService {

	@Autowired
	private CamionDAO operacion;
	@Override
	public void registrar(Camion x,long lote) {
		// TODO Auto-generated method stub
		x.setLote(lote);
		operacion.save(x);
	}

	@Override
	public List<Camion> listar(long lote) {
		// TODO Auto-generated method stub
		List<Camion> listaCamionesPorLote=new LinkedList<Camion>();
		for(Camion x:operacion.findAll()) {
			if(x.getLote()==lote)
			listaCamionesPorLote.add(x);	
		}
		return listaCamionesPorLote;
	}

	
}
