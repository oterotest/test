package com.oterotest.prueba.control.rest;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oterotest.prueba.control.servicio.ICamionService;
import com.oterotest.prueba.modelo.Camion;

@CrossOrigin(origins="http://127.0.0.1:5500")
@RestController
@RequestMapping("/kometsales")
public class RestCamion {
 
	@Autowired
	private ICamionService operacion;
	
	@GetMapping("camiones/all")
	public List<Camion> listar(@PathParam(value="lote") int lote){
		return operacion.listar(lote);
		
	}
	
}
