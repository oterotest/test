package com.oterotest.prueba.control.dao;

import org.springframework.data.repository.CrudRepository;

import com.oterotest.prueba.modelo.Cargue;

public interface CargueDAO  extends CrudRepository<Cargue, Long>{

}
