package com.oterotest.prueba.control.servicio;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.oterotest.prueba.control.dao.CargueDAO;
import com.oterotest.prueba.modelo.Cargue;

@Service
public class CargueImplService implements ICargueService {
	@Autowired
	private CargueDAO operacion;


	@Override
	public Cargue guardar(MultipartFile x, String mail) throws IOException {
		
		
		// TODO Auto-generated method stub
		Cargue informacionCarga = new Cargue();
		informacionCarga.setFecha_cargue(LocalDateTime.now());
		informacionCarga.setNombre_archivo(x.getName());
		informacionCarga.setDatos(x.getBytes());
		informacionCarga.setTipo_archivo(x.getContentType());
		informacionCarga.setMail_responsable(mail);

		operacion.save(informacionCarga);
		return informacionCarga;
	}

	@Override
	public void modificar(Cargue cargue, long id) {
		// TODO Auto-generated method stub
		operacion.findById(id).ifPresent((x) -> {
			cargue.setId(id);
			operacion.save(cargue);
		});
	}

}
