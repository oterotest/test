package com.oterotest.prueba.control.servicio;
import java.util.List;

import com.oterotest.prueba.modelo.Camion;
public interface ICamionService {

	public void registrar(Camion x,long lote);
	
	public List<Camion> listar(long lote);
	
}
