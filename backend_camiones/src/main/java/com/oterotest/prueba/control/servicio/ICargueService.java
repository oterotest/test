package com.oterotest.prueba.control.servicio;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.oterotest.prueba.modelo.Cargue;

public interface ICargueService {

	public Cargue guardar(MultipartFile  x, String mail) throws IOException, InterruptedException ;
	
	public void modificar(Cargue x, long id);
}
