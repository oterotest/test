package com.oterotest.prueba.control.dao;

import org.springframework.data.repository.CrudRepository;

import com.oterotest.prueba.modelo.Camion;

public interface CamionDAO extends CrudRepository<Camion, Long>{

}
