package com.oterotest.prueba.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="cargue")
public class Cargue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	private String nombre_archivo;
	@NotNull
	private String tipo_archivo;
	@NotNull
	private String mail_responsable;
	public Cargue(@NotNull String nombre_archivo, @NotNull String tipo_archivo, @NotNull String mail_responsable,
			@NotNull byte[] datos, @NotNull int numero_exitos, @NotNull int numero_error,
			@NotNull LocalDateTime fecha_cargue) {
		super();
		this.nombre_archivo = nombre_archivo;
		this.tipo_archivo = tipo_archivo;
		this.mail_responsable = mail_responsable;
		this.datos = datos;
		this.numero_exitos = numero_exitos;
		this.numero_error = numero_error;
		this.fecha_cargue = fecha_cargue;
	}

	@Lob
	@NotNull
	private byte [] datos;
	@NotNull
	private int numero_exitos;
	@NotNull
	private int numero_error;
	@NotNull
	private LocalDateTime fecha_cargue;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre_archivo() {
		return nombre_archivo;
	}

	public void setNombre_archivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}

	public String getTipo_archivo() {
		return tipo_archivo;
	}

	public void setTipo_archivo(String tipo_archivo) {
		this.tipo_archivo = tipo_archivo;
	}

	public int getNumero_exitos() {
		return numero_exitos;
	}

	public void setNumero_exitos(int numero_exitos) {
		this.numero_exitos = numero_exitos;
	}

	public int getNumero_error() {
		return numero_error;
	}

	public void setNumero_error(int numero_error) {
		this.numero_error = numero_error;
	}

	public LocalDateTime getFecha_cargue() {
		return fecha_cargue;
	}

	public void setFecha_cargue(LocalDateTime fecha_cargue) {
		this.fecha_cargue = fecha_cargue;
	}
   @NotNull
	public byte[] getDatos() {
		return datos;
	}

	public void setDatos(byte[] datos) {
		this.datos = datos;
	}


	public String getMail_responsable() {
		return mail_responsable;
	}

	public void setMail_responsable(String mail_responsable) {
		this.mail_responsable = mail_responsable;
	}

	public Cargue() {
		super();
	}


}
