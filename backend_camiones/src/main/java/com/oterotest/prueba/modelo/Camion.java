package com.oterotest.prueba.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

@Entity
@Table (name="camion")
public class Camion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private long id;
	@CsvBindByName
	@NotEmpty
	private String placa;
	@CsvBindByName
	@NotNull
	private String marca;
	@CsvBindByName
	@NotNull
	private int modelo;
	@CsvDate(value="dd/MM/yyyy")
	@CsvBindByName
	@NotNull
	private Date fecha_compra =Calendar.getInstance().getTime();
	@NotNull
	@CsvBindByName
	private float capacidad_carga;
	@NotNull
	private long lote;
	
	
	public long getLote() {
		return lote;
	}
	public void setLote(long lote) {
		this.lote = lote;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public Date getFecha_compra() {
		return fecha_compra;
	}
	public void setFecha_compra(Date fecha_compra) {
		this.fecha_compra = fecha_compra;
	}
	public float getCapacidad_carga() {
		return capacidad_carga;
	}
	public void setCapacidad_carga(float capacidad_carga) {
		this.capacidad_carga = capacidad_carga;
	}
	public Camion(@NotEmpty String placa, @NotNull String marca, @NotNull int modelo, @NotNull Date fecha_compra,
			@NotNull float capacidad_carga, @NotNull long lote) {
		super();
		this.placa = placa;
		this.marca = marca;
		this.modelo = modelo;
		this.fecha_compra = fecha_compra;
		this.capacidad_carga = capacidad_carga;
		this.lote=lote;
	}
	public Camion() {
		super();
	}
	
	
	

}
